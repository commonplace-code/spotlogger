#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import Dict, List

import requests
import dateutil
from dateutil import parser
from datetime import datetime, date, timedelta
from flask import Flask, render_template
from flask_bootstrap import Bootstrap

URL_GYM_LIST = "https://api.toplogger.nu/v1/gyms.json"
URL_RESERVATION_AREAS = "https://api.toplogger.nu/v1/gyms/{gym}/reservation_areas"
URL_RESERVATION_SLOTS = "https://api.toplogger.nu/v1/gyms/{gym}/slots?date={date}&reservation_area_id={area}&slim=true"

FAVOURITE_GYM_ID_NAMES = [
    'sterk',
    'rock-steady-bussum',
    'energiehaven'
]

EXCLUDE_AREAS = [
    'shop',
    'fysiofabriek spreekuur',
    'klimmen'
]


def retrieve_gyms():
    url = URL_GYM_LIST
    response = requests.request("GET", url)
    gyms = response.json()

    def set_favourite(_gym):
        _gym['favourite'] = True if _gym['slug'] in FAVOURITE_GYM_ID_NAMES else False
        return _gym

    # filter on NL gyms and sort on favourites
    return  sorted([set_favourite(gym) for gym in gyms if gym['country'] == 'NL'],
                   key=lambda k: k['favourite'],
                   reverse=True)


NL_GYMS = retrieve_gyms()
GYM_BY_ID = {gym['id']: gym for gym in NL_GYMS}

app = Flask(__name__)
bootstrap = Bootstrap(app)


def retrieve_areas(gym_id: int) -> List:
    url = URL_RESERVATION_AREAS.format(gym=gym_id)
    response = requests.request("GET", url)
    areas = response.json()

    return areas


def retrieve_open_spots(gym_id: int, area_id: int, days: int=1) -> (int, Dict):
    _days = []
    total_spots = 0

    for day in range(0, days):
        _day = date.today() + timedelta(days=day)
        url = URL_RESERVATION_SLOTS.format(gym=gym_id, area=area_id, date=str(_day))
        response = requests.request("GET", url)

        spots = {'day': _day, 'spots_available': 0, 'spots': []}

        for slot in response.json():
            if slot['require_password']:
                continue

            available = slot['spots'] - slot['spots_booked']

            now = datetime.now()
            start_at = dateutil.parser.isoparse(slot['start_at'])
            end_at = dateutil.parser.isoparse(slot['end_at'])

            if now.timestamp() > end_at.timestamp():
                continue

            if available > 0:
                spot = {
                    'start_at': start_at,
                    'end_at': end_at,
                    'details': slot['details'],
                    'spots': available,
                }

                spots['spots'].append(spot)
                spots['spots_available'] += available

        total_spots += spots['spots_available']
        _days.append(spots)

    return total_spots, _days


@app.template_filter()
def dayFormat(day:datetime):
    day = day.strftime('%A %d %B %Y')
    return day


@app.template_filter()
def timeFormat(time:datetime):
    time = time.strftime('%H:%M')
    return time


@app.route("/")
def index():
    return render_template("index.html", gyms=NL_GYMS)


@app.route("/<int:id>")
@app.route("/<int:id>/")
@app.route("/<int:id>/<int:days>")
def get_open_slots(id, days=1):
    if not id in GYM_BY_ID:
        return f'Unknown gym id: {id}'

    until = date.today() + timedelta(days=days-1)
    areas = retrieve_areas(id)
    spots = {'gym': GYM_BY_ID[id]['name'],
             'total_available': 0,
             'areas': []}

    for area in areas:
        if not area['name'].lower() in EXCLUDE_AREAS:
            total_spots, available = retrieve_open_spots(gym_id=id, area_id=area['id'], days=days)
            if available:
                spots['areas'].append({'name': area['name'], 'days': available})
                spots['total_available'] += total_spots

    return render_template('spots.html', spots=spots, current_id=id, gyms=NL_GYMS, days=days, until=until)


if __name__ == "__main__":
    app.run()
